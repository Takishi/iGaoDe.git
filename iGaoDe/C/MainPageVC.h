//
//  MainPageVC.h
//  iGaoDe
//
//  Created by YXF on 2018/5/4.
//  Copyright © 2018年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>

@interface MainPageVC : UIViewController<AMapLocationManagerDelegate,MAMapViewDelegate>

@property (nonatomic,strong)MAMapView *mapView;

@end
